package com.itau.marketplace.controllers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.marketplace.models.Anuncio;
import com.itau.marketplace.models.AnuncioUsuario;
import com.itau.marketplace.repositories.AnuncioRepository;
import com.itau.marketplace.repositories.AnuncioUsuarioRepository;


@RestController
public class AnuncioController {

	@Autowired
	AnuncioRepository anuncioRepository;
	
	@Autowired
	AnuncioUsuarioRepository anuncioUsuarioRepository;
	
	@RequestMapping(method=RequestMethod.POST, path="/anuncio")
	public Anuncio criar(@RequestBody Anuncio anuncio) {
		anuncio.setId(UUID.randomUUID());
		anuncio.setData(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));	
		
		AnuncioUsuario anuncioUsuario = new AnuncioUsuario();
		anuncioUsuario.setData(anuncio.getData());
		anuncioUsuario.setDescricao(anuncio.getDescricao());
		anuncioUsuario.setTitulo(anuncio.getTitulo());
		anuncioUsuario.setPreco(anuncio.getPreco());
		anuncioUsuario.setUsername(anuncio.getUsername());
		anuncioUsuario.setAnuncioid(anuncio.getId());

		anuncio = anuncioRepository.save(anuncio);
		anuncioUsuarioRepository.save(anuncioUsuario);
		
		return anuncio;	
	}
	
	@RequestMapping(path="/anuncios")
	public Iterable<Anuncio> buscarAnuncio(){
		return anuncioRepository.findAll();
	}
	

}
