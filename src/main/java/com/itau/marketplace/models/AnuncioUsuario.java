package com.itau.marketplace.models;

import java.util.UUID;


import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class AnuncioUsuario {
	
	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private String username;
	
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private UUID anuncioid;
	
	private String titulo;
	private String descricao;
	private double preco;
	private String data;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	public UUID getAnuncioid() {
		return anuncioid;
	}
	public void setAnuncioid(UUID anuncioid) {
		this.anuncioid = anuncioid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	
}
