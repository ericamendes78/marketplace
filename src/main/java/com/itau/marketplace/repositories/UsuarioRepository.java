package com.itau.marketplace.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.marketplace.models.AnuncioUsuario;
import com.itau.marketplace.models.Usuario;


public interface UsuarioRepository extends CrudRepository<Usuario, UUID> {
	
	public Optional<Usuario> findByUsername(String username);

}
